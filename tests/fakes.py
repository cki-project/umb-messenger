"""Fake DW data."""
from unittest.mock import MagicMock


def get_fake_checkout(builds=[], attributes={}, misc={}, actual_attrs={}):
    """Get a fake checkout."""
    checkout = MagicMock()
    checkout.builds.list.return_value = builds
    checkout.attributes = attributes
    checkout.attributes['misc'] = misc
    checkout.misc = misc

    for key, value in actual_attrs.items():
        setattr(checkout, key, value)

    return checkout


def get_fake_build(tests=[], attributes={}, misc={}, actual_attrs={}):
    """Get a fake build."""
    build = MagicMock()
    build.tests.list.return_value = tests
    build.attributes = attributes
    build.attributes['misc'] = misc
    build.misc = misc

    for key, value in actual_attrs.items():
        setattr(build, key, value)

    return build


def get_fake_test(issues=[], actual_attrs={}):
    """Get a fake test."""
    test = MagicMock()
    test.issues_occurrences.list.return_value = issues

    for key, value in actual_attrs.items():
        setattr(test, key, value)

    return test


def get_fake_issue_occurrence(is_regression=False):
    """Get a fake issue."""
    issue = MagicMock()
    issue.is_regression = is_regression

    return issue


class FakeMergeRequest:
    """Fake merge request."""

    def __init__(self, attributes):
        """Initialize."""
        for key, value in attributes.items():
            setattr(self, key, value)
